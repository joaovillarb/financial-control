FROM openjdk:17-slim

WORKDIR /AppServer
ADD target/*.jar financial-control.jar

ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -jar /AppServer/financial-control.jar"]
